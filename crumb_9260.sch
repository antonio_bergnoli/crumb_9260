EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:crumb_9260-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L MT48LC4M16A2P IC2
U 1 1 57120677
P 7450 2600
F 0 "IC2" H 6850 3800 50  0000 C CNN
F 1 "MT48LC4M16A2P" H 8100 1400 50  0000 C CNN
F 2 "TSOPII-54" H 7450 2600 50  0000 C CIN
F 3 "" H 7450 2350 50  0000 C CNN
	1    7450 2600
	1    0    0    -1  
$EndComp
$Comp
L MT48LC4M16A2P IC1
U 1 1 5712070A
P 4550 2600
F 0 "IC1" H 3950 3800 50  0000 C CNN
F 1 "MT48LC4M16A2P" H 5200 1400 50  0000 C CNN
F 2 "TSOPII-54" H 4550 2600 50  0000 C CIN
F 3 "" H 4550 2350 50  0000 C CNN
	1    4550 2600
	1    0    0    -1  
$EndComp
$Comp
L AT91SAM9260 U1
U 6 1 57140513
P 1350 4200
F 0 "U1" H 1550 4450 60  0000 L CNN
F 1 "AT91SAM9260" H 1550 4350 60  0000 L CNN
F 2 "" H 1350 4200 60  0000 C CNN
F 3 "" H 1350 4200 60  0000 C CNN
	6    1350 4200
	1    0    0    -1  
$EndComp
$EndSCHEMATC
